package com.moviles2.repaso01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener { //tiene los elementos para que la aplicacion comiencew

    EditText etUser,etPassword;
    Button btnlogin,btnRegister;

    @Override //polimorfismo o tiene clase abstracta
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  // En la herencia
        setContentView(R.layout.activity_main);

        etUser=findViewById(R.id.etUser);
        etPassword=findViewById(R.id.etPassword);
        btnlogin= findViewById(R.id.btnlogin);
        btnRegister= findViewById(R.id.btnRegister);
        btnlogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    public void Login(View view) {
        Toast.makeText(this, "Inicio de sesión", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnlogin:
              Toast.makeText(this, "metodo login", Toast.LENGTH_SHORT).show();
              break;
            case R.id.btnRegister:
                Toast.makeText(this, "metodo Register", Toast.LENGTH_SHORT).show();
                break;

             }
    }
}


