package com.moviles2.technoshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.moviles2.technoshop.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding loginBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginBinding = ActivityLoginBinding.inflate(getLayoutInflater());
        View v = loginBinding.getRoot();
        setContentView(v);
        saveUserPreferences(this);
    }

    public void saveUserPreferences(Context context){

        // se obtiene la data del usuario desde firestore
        String email = "juan@gmail.com";
        String role = "vendedor";
        String name = "juan";
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.user_preference_key), Context.MODE_PRIVATE);
        // permite escribir data en las shared preferences
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("status", true);
        editor.putString("email", email);
        editor.putString("role", role);
        editor.putString("name", name);
        editor.commit();
    }
}