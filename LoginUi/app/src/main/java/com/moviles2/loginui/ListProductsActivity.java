package com.moviles2.loginui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.moviles2.loginui.databinding.ActivityListProductsBinding;
import com.moviles2.loginui.entities.ProductEntity;

import java.util.ArrayList;
import java.util.List;

public class ListProductsActivity extends AppCompatActivity {

    private ActivityListProductsBinding listProductsBinding;

    List<ProductEntity> arrayProducts = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listProductsBinding = ActivityListProductsBinding.inflate(getLayoutInflater());
        View view = listProductsBinding.getRoot();
        setContentView(view);
    }
}